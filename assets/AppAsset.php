<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $images = [
        'images/optidev.png',
        'images/optidev-test.png',
        'images/web.png',
        'images/software.png',
        'images/consulting.png',
        'images/design.png',
        'images/maintenance.png',
        'images/services_icons.png',
        'images/webservice.jpeg',
        'images/web-responsive.png',
        'images/web-reputation.png',
        'images/web-com.png',
        'images/soft-time.png',
        'images/soft-decision.png',
        'images/soft-reliable.png',
        'images/cons-outside.png',
        'images/cons-expertise.png',
        'images/cons-budget.png',
        'images/des-logo.png',
        'images/des-flyers.png',
        'images/des-graphics.png',
        'images/services_icons.png',
    ];
    public $js = [
      'js/site.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
