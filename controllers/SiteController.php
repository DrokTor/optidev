<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
     public function actionLang($lang)
     {
       if($lang=='fr')
       {
         Yii::$app->session->set('language', 'fr_FR');
       }
       else {
         Yii::$app->session->set('language', 'en-US');
         //die(Yii::$app->language);
       }
         return $this->redirect('/index');
     }


    public function actionIndex()
    {
         return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
     /*


    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['infoEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionHarmonyqms()
    {

        return $this->render('harmony');
    }
    public function actionWeb()
    {

        return $this->render('web');
    }
    public function actionSoftware()
    {

        return $this->render('software');
    }
    public function actionGraphics()
    {

        return $this->render('graphics');
    }
    public function actionConsulting()
    {

        return $this->render('consulting');
    }
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionProducts()
    {
        return $this->render('products');
    }
}
