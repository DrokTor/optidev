<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Website design';
?>
<div class="site-page">
    <h1><?= Html::encode($this->title) ?></h1>

    <!--<h2>Websites</h2>-->
    <p class="col-sm-6">
      Today your website plays an important role in your business strategy, increase your business opportunities by offering a web experience for your clients which reflects your unique identity,
      whether you simply want to provide general information or promote a new product, we make you use the power of the web to gain new perspectives in your business.
    </p><br>
    <img class="col-sm-offset-2 col-sm-4" src="images/webservice.jpeg"/>
    <div class="row sep">...</div>
    <div class="text-center">
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Improve communication</h3><img src="/images/web-com.png" class="col-sm-12"/><p>Offer your clients an efficient way of getting information about your products and services.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Responsive design</h3><img src="/images/web-responsive.png" class="col-sm-12"/><p>Widen your accessibility with a responsive design that adapts to wide screens, tablets and smartphones.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Increase reputation</h3><img src="/images/web-reputation.png" class="col-sm-12"/><p>Show your clients that they can trust you and boost your reputation.</p></div>
    </div>


</div>
