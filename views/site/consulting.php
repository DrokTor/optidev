<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Consulting';
?>
<div class="site-page">
    <h1><?= Html::encode($this->title) ?></h1>


    <p class="col-sm-6">
      Every day we are confronted with challenges that are beyond our area of expertise, we help you face them and empower you to get the most out of your valuable time.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/consultingservice.jpeg"/>
    <div class="sep">...</div>
    <div class="text-center">
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Expertise</h3><img src="/images/cons-expertise.png" class="col-sm-12"/><p>Benefit from our large expertise and make sure you are on the right path.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Budget efficient</h3><img src="/images/cons-budget.png" class="col-sm-12"/><p>Spend only when you need to, no need to hire a full time employee and stay within your budget.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Outside view</h3><img src="/images/cons-outside.png" class="col-sm-12"/><p>Get an outside view on your subject that helps you if you miss something.</p></div>
    </div>

</div>
