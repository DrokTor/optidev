<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Harmony QMS - Système de gestion de la qualité';
?>
<div class="site-page">
    <h1>Harmony QMS</h1> <?php /*Html::encode($this->title) */ ?>


    <p class="col-sm-6">
      Maitrisez vos challenges de gestion de la qualité et préservez votre énergie pour ce qui compte le plus, Harmony QMS supprime toute la complexité du processus de gestion de la qualité et vous offre un système efficace, économique et facile à utiliser.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/harmony_interface.png"/>
    <div class="sep">...</div>
    <div class="text-center">
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Économique</h3><img src="/images/soft-time.png" class="col-sm-12"/><p>Débarrassez-vous des tâches qui vous consomment du temps et améliorez votre performance.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Qualité et fiabilité</h3><img src="/images/soft-reliable.png" class="col-sm-12"/><p>Informatisez votre flux de travail et profitez d'une fiabilité et qualité augmentées.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>De meilleures décisions</h3><img src="/images/soft-decision.png" class="col-sm-12"/><p>Le traitement de données vous facilitera une meilleure prise de décision et vous dévoilera de nouvelles perspectives de développement.</p></div>
    </div>
    <div class="sep">...</div>
    <h2><?= Html::encode("Deviation & CAPA management")  ?></h2>
    <div class="row">
      <div class="col-sm-12">
    <p class="col-sm-6">
       &nbsp Harmony Deviation & CAPA Module® automatise et gère le processus de documentation, d'investigation et de résolution des déviations par rapport aux procédures et spécifications écrites. Cette solution permet de résoudre efficacement les déviations et d'utiliser les données
Recueillies comme base pour l'amélioration continue de la qualité. L'application est conçue aussi pour automatiser et gérer efficacement le processus CAPA.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/deviation.png"/>
  </div>
    </div>
    <div class="row sep">...</div>
    <h2>Change control</h2>
    <p class="col-sm-6">
       &nbsp Harmony Change Control Module® est conçu pour automatiser et gérer efficacement chaque étape du processus de maitrise des changements allant de la description, l'approbation, la mise en œuvre, la vérification et la clôture du projet.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/change.png"/>

    <div class="sep">...</div>
    <h2>Audit management</h2>
    <p class="col-sm-6">
       &nbsp Harmony Audit Module® vous aide à gérer efficacement les audits et les inspections impliquant votre organisation, en planifiant et programmant les audits et les ressources, en saisissant les résultats, en rapportant les données obtenues et en gérant les réponses et les actions correctives / préventives.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/audit.png"/>

    <div class="sep">...</div>
    <h2>Complaint management</h2>
    <p class="col-sm-6">
       &nbsp Harmony Customer Complaints Module® est une solution facile à utiliser qui vous aidera à automatiser la gestion des réclamations, à l'intégrer au système de qualité et à gérer plus efficacement leur résolution.
     </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/complaint.png"/>

    <div class="sep">...</div>
    <h2>Training management</h2>
    <p class="col-sm-6">
       &nbsp Harmony Staff Qualification Module® vous aide à gérer efficacement le processus de Qualification de votre personnel, en planifiant, assignant et faisant le suivi de toutes les Certifications, SOPs, Formations et Compétences requises.
     </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/training.png"/>

    <div class="sep">...</div>


</div>
