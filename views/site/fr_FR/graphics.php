<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Design graphique';
?>
<div class="site-page">
    <h1><?= Html::encode($this->title) ?></h1>


     <p class="col-sm-6">
      Communiquez avec vos clients avec des graphismes soignés qui parlent en votre nom et allumez leur désir de travailler avec vous.</p>
    <img class="col-sm-offset-2 col-sm-4" src="images/designservices.png"/>
    <div class="sep">...</div>
    <div class="text-center">
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Logo</h3><img src="/images/des-logo.png" class="col-sm-12"/><p>Donnez-vous une identité unique avec un logo de marque taillé qui captive vos clients.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Marketing</h3><img src="/images/des-flyers.png" class="col-sm-12"/><p>Impressionnez votre cible avec un matériel exceptionnel conçu spécifiquement pour laisser un impact chez vos clients.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Graphiques</h3><img src="/images/des-graphics.png" class="col-sm-12"/><p>Donnez à votre matériel un nouveau souffle avec des graphiques qui vous simplifient la transmission du message.</p></div>
    </div>


</div>
