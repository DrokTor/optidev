<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Développement de logiciels';
?>
<div class="site-page">
    <h1><?= Html::encode($this->title) ?></h1>


    <p class="col-sm-6">
      Augmentez votre productivité en exploitant la puissance des logiciels et optimisez votre travail dans un flux informatisé.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/softwareservice.jpeg"/>
    <div class="sep">...</div>
    <div class="text-center">
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Économique</h3><img src="/images/soft-time.png" class="col-sm-12"/><p>Débarrassez-vous des tâches qui vous consomment du temps et améliorez votre performance.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Qualité et fiabilité</h3><img src="/images/soft-reliable.png" class="col-sm-12"/><p>Informatisez votre flux de travail et profitez d'une fiabilité et qualité augmentées.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>De meilleures décisions</h3><img src="/images/soft-decision.png" class="col-sm-12"/><p>Le traitement de données vous facilitera une meilleure prise de décision et vous dévoilera de nouvelles perspectives de développement.</p></div>
    </div>

</div>
