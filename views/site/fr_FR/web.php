<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Website design';
?>
<div class="site-page">
    <h1><?= Html::encode($this->title) ?></h1>

    <!--<h2>Websites</h2>-->
    <p class="col-sm-6">
      Votre site web aujourd'hui joue un rôle important dans votre stratégie commerciale, améliorez vos opportunités d'affaires en offrant une expérience à vos clients qui reflète votre identité unique, que vous voulez simplement fournir des informations générales ou promouvoir un nouveau produit ou service, on vous fait bénéficier du pouvoir du web pour conquérir de nouvelles perspectives dans votre entreprise.
  </p><br>
    <img class="col-sm-offset-2 col-sm-4" src="images/webservice.jpeg"/>
    <div class="row sep">...</div>
    <div class="text-center">
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Améliorer la communication</h3><img src="/images/web-com.png" class="col-sm-12"/><p>Offrez à vos clients un moyen efficace pour obtenir des informations à propos de vos produits et services.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Responsive design</h3><img src="/images/web-responsive.png" class="col-sm-12"/><p>Élargissez votre accessibilité avec un design qui s'adapte aux écrans larges, tablettes et smartphones.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Augmenter la réputation</h3><img src="/images/web-reputation.png" class="col-sm-12"/><p>Montrez à vos clients qu'ils peuvent vous faire confiance et boostez votre réputation.</p></div>
    </div>


</div>
