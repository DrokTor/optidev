<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Consulting';
?>
<div class="site-page">
    <h1><?= Html::encode($this->title) ?></h1>


    <p class="col-sm-6">
      Chaque jour, nous sommes confrontés à des défis qui sont hors de notre domaine d'expertise, nous vous aidons à les affronter et vous donnons les moyens de tirer le meilleur parti de votre temps précieux.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/consultingservice.jpeg"/>
    <div class="sep">...</div>
    <div class="text-center">
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Expertise</h3><img src="/images/cons-expertise.png" class="col-sm-12"/><p>Bénéficiez de notre large expertise et soyez sûr d'être sur le bon chemin.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Efficacité budgétaire</h3><img src="/images/cons-budget.png" class="col-sm-12"/><p>Dépensez seulement quand vous en aurez besoin, pas besoin d'embaucher un employé à temps plein et restez dans votre budget.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Vue extérieure</h3><img src="/images/cons-outside.png" class="col-sm-12"/><p>Sollicitez une vue d'extérieur sur votre sujet qui vous aide si vous oubliez quelque chose.</p></div>
    </div>

</div>
