<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Services';
?>
<div class="site-page">
    <!--<h1><?= Html::encode($this->title) ?></h1>--->

    <h2>Sitewebs</h2>
    <p class="col-sm-6">
      Votre site web aujourd'hui est votre business, augmentez vos opportunités en business en offrant une expérience à vos clients qui reflète votre unique identité,
       que vous voulez simplement fournir des informations générales ou promouvoir un nouveau produit ou service, on vous fait bénéficier du pouvoir du web pour conquérir de nouvelles perspectives dans votre entreprise.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/webservice.jpeg"/>
    <div class="sep">...</div>
    <h2>Logiciels</h2>
    <p class="col-sm-6">
      Augmentez votre productivité en exploitant la puissance des applications logicielles et optimiser votre travail dans un flux de travail informatisé.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/softwareservice.jpeg"/>
    <div class="sep">...</div>
    <h2>Design</h2>
    <p class="col-sm-6">
      Communiquez avec vos clients avec des graphismes soignés qui parlent en votre nom et leur faire sentir le désir de travailler avec vous.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/designservice.png"/>
    <div class="sep">...</div>
    <h2>Consulting</h2>
    <p class="col-sm-6">
      Chaque jour, nous sommes confrontés à des défis qui sont hors de notre domaine d'expertise, nous vous aidons à les affronter et on vous donne les moyens de tirer le meilleur parti de votre temps précieux.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/consultingservice.jpeg"/>


</div>
