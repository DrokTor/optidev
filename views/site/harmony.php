<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Harmony QMS - Quality Management System';
?>
<div class="site-page">
    <h1>Harmony QMS</h1> <?php /*Html::encode($this->title) */ ?>


    <p class="col-sm-6">
      Master your quality challenges and save your energy for only what matters most, Harmony QMS takes away all the complexity of the quality management process and provide you with an efficient, time saving and easy to use system.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/harmony_interface.png"/>
    <div class="sep">...</div>
    <div class="text-center">
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Time efficient</h3><img src="/images/soft-time.png" class="col-sm-12"/><p>Get rid of time wasting tasks and increase your performance.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Quality and reliability</h3><img src="/images/soft-reliable.png" class="col-sm-12"/><p>Computerize your workflow and enjoy increased reliability and quality.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Better decisions</h3><img src="/images/soft-decision.png" class="col-sm-12"/><p>Data processing facilitates decision making and uncover new perspectives to improve.</p></div>
    </div>
    <div class="sep">...</div>
    <h2><?= Html::encode("Deviation & CAPA management")  ?></h2>
    <p class="col-sm-6">
       &nbsp Harmony Deviation & CAPA Module® automates and manages the process of documenting, investigating, and resolving deviations from written procedures and specifications.
      Its flexible design also allows you to automate and effecively manage the CAPA process.
       This solution provides the capability to resolve a deviation efficiently and to harness collected data
       as a basis for continuous quality improvement.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/deviation.png"/>

    <div class="sep">...</div>
    <h2>Change control</h2>
    <p class="col-sm-6">
       &nbsp Harmony Change Control Module® is designed to automate and effectively manage every step of the change control process, from
submission through actual implementation, verification, and close of project.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/change.png"/>

    <div class="sep">...</div>
    <h2>Audit management</h2>
    <p class="col-sm-6">
       &nbsp Harmony Audit Module® helps you effectively manage audits and inspections involving your organization, by planning and scheduling audits and resources, capturing of findings, reporting on resulting data and management of responses and corrective/preventive actions (CAPAs).
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/audit.png"/>

    <div class="sep">...</div>
    <h2>Complaint management</h2>
    <p class="col-sm-6">
       &nbsp Harmony Customer Complaints Module® is an easy-to-use solution that will help you automate complaint handling, integrate it with the quality system, and manage complaints resolution more effectively.</p>
    <img class="col-sm-offset-2 col-sm-4" src="images/complaint.png"/>

    <div class="sep">...</div>
    <h2>Training management</h2>
    <p class="col-sm-6">
       &nbsp Harmony Staff Qualification Module® helps you effectively manage the Qualification of your Staff process, by planning, assigning and tracking all Certifications, SOPs, Trainings and Competencies related to a Job Position.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/training.png"/>

    <div class="sep">...</div>


</div>
