<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Software development';
?>
<div class="site-page">
    <h1><?= Html::encode($this->title) ?></h1>


    <p class="col-sm-6">
      Increase your productivity by harnessing the power of software applications and optimize your work in a computerized workflow.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/softwareservice.jpeg"/>
    <div class="sep">...</div>
    <div class="text-center">
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Time efficient</h3><img src="/images/soft-time.png" class="col-sm-12"/><p>Get rid of time wasting tasks and increase your performance.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Quality and reliability</h3><img src="/images/soft-reliable.png" class="col-sm-12"/><p>Computerize your workflow and enjoy increased reliability and quality.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Better decisions</h3><img src="/images/soft-decision.png" class="col-sm-12"/><p>Data processing facilitates decision making and uncover new perspectives to improve.</p></div>
    </div>

</div>
