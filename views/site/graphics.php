<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Graphic design';
?>
<div class="site-page">
    <h1><?= Html::encode($this->title) ?></h1>


     <p class="col-sm-6">
      Communicate with your clients with neat graphics that speak on your behalf and make them feel the desire to work with you.

    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/designservices.png"/>
    <div class="sep">...</div>
    <div class="text-center">
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Logo</h3><img src="/images/des-logo.png" class="col-sm-12"/><p>Give yourself a unique identity with a tailored brand logo, which captivates your clients.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Marketing</h3><img src="/images/des-flyers.png" class="col-sm-12"/><p>Impress your target with outstanding material designed specifically to leave an impact on your clients.</p></div>
      <div class="col-sm-4 ccol-sm-offset-1 interactive-box"><h3>Graphics</h3><img src="/images/des-graphics.png" class="col-sm-12"/><p>Let your materials get a second breath with graphics that simplify the message delivery.</p></div>
    </div>


</div>
