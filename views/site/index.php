


<?php
use evgeniyrru\yii2slick\Slick;
use yii\web\JsExpression;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->registerLinkTag([
    'rel' => 'stylesheet',
    'type' => 'text/css',
    'href' => '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css',
]);
$this->title = 'OptiDev';
?>

<div class="hidden-xs full-height">

<?=Slick::widget([

        // HTML tag for container. Div is default.
        'itemContainer' => 'div',

        // HTML attributes for widget container
        'containerOptions' => ['class' => 'screen row'],

        // Items for carousel. Empty array not allowed, exception will be throw, if empty
        'items' => [
            '<h2>Web developpment</h2><p>Today your website plays an important role in your business strategy, increase your business opportunities ... <span class="readmore"><a href="/web">More</a></span> </p>'.Html::img('/images/web.jpg',['class'=>"images"]),
            '<h2>Software developpment</h2><p>Increases your productivity by harnessing the power of software applications and optimize your work in a computerized workflow ... <span class="readmore"><a href="/software">More</a></span>  </p>'.Html::img('/images/software.jpeg',['class'=>"images"]),
            '<h2>Graphic design</h2><p> Communicate with your clients with neat graphics ... <span class="readmore"><a href="/graphics">More</a></span></p>'.Html::img('/images/design.jpg',['class'=>"images"]),
            '<h2>Consulting IT</h2><p>Every day we are confronted with challenges that out of our area of expertise, we can prevent you from wasting your valuable time ... <span class="readmore"><a href="/consulting">More</a></span></p>'.Html::img('/images/consulting.jpeg',['class'=>"images"]),
            //'<h2>Maintenance</h2><p>The world of computing is full of suprises, sometimes bad suprises, let us take care of what we do best so you can focus o nwhat you do best ... <span class="readmore"><a href="/services">More</a></span></p><img data-lazy="/images/maintenance.jpeg"/>,

        ],

        // HTML attribute for every carousel item
        'itemOptions' => ['class' => 'carItem'],

        // settings for js plugin
        // @see http://kenwheeler.github.io/slick/#settings
        'clientOptions' => [
            'autoplay' => true,
            'arrows'=> false,
            'dots'     => true,
            'fade'=>true,
            'lazyLoad'=> 'ondemand',
            'speed'=>2000,
            // note, that for params passing function you should use JsExpression object
            'onAfterChange' => new JsExpression('function() {console.log("The cat has shown")}'),
            //'onSwipe' => new JsExpression('function() {console.log("The cat has shown")}'),

        ],

    ]); ?>
  </div>
  <div class="visible-xs full-height">

    <?=Slick::widget([

            // HTML tag for container. Div is default.
            'itemContainer' => 'div',

            // HTML attributes for widget container
            'containerOptions' => ['class' => 'screen-xs row'],

            // Items for carousel. Empty array not allowed, exception will be throw, if empty
            'items' => [
                '<h2>Web</h2><p>Today your website plays an important role in your business strategy, increase your business opportunities ... <span class="readmore"><a href="/web">More</a></span> </p>'.Html::img('/images/web.jpg',['class'=>"images"]),
                '<h2>Software developpment</h2><p>Increases your productivity by harnessing the power of software applications and optimize your work in a computerized workflow ... <span class="readmore"><a href="/software">More</a></span>  </p>'.Html::img('/images/software.jpeg',['class'=>"images"]),
                '<h2>Design</h2><p> Communicate with your clients with neat graphics ... <span class="readmore"><a href="/graphics">More</a></span></p>'.Html::img('/images/design.jpg',['class'=>"images"]),
                '<h2>Consulting</h2><p>Every day we are confronted with challenges that out of our area of expertise, we can prevent you from wasting your valuable time ... <span class="readmore"><a href="/consulting">More</a></span></p>'.Html::img('/images/consulting.jpeg',['class'=>"images"]),
                //'<h2>Maintenance</h2><p>The world of computing is full of suprises, sometimes bad suprises, let us take care of what we do best so you can focus o nwhat you do best ... <span class="readmore"><a href="/services">More</a></span></p><img data-lazy="/images/maintenance.jpeg"/>,

            ],

            // HTML attribute for every carousel item
            'itemOptions' => ['class' => 'carItem'],

            // settings for js plugin
            // @see http://kenwheeler.github.io/slick/#settings
            'clientOptions' => [
                'autoplay' => true,
                'arrows'=> false,
                'dots'     => true,
                'fade'=>true,
                'lazyLoad'=> 'ondemand',
                'speed'=>2000,
                // note, that for params passing function you should use JsExpression object
                'onAfterChange' => new JsExpression('function() {console.log("The cat has shown")}'),
                //'onSwipe' => new JsExpression('function() {console.log("The cat has shown")}'),

            ],

        ]); ?>
  </div>
