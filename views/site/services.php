<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Services';
?>
<div class="site-page">
    <!--<h1><?= Html::encode($this->title) ?></h1>--->

    <h2>Websites</h2>
    <p class="col-sm-6">
      Your website is your business today, increase your business opportunities by offering a web experience for your clients which reflects your unique identity,
      whether you simply want to provide general information or promote a new product, we make you use the power of the web to gain new perspectives in your business.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/webservice.jpeg"/>
    <div class="sep">...</div>
    <h2>Software applications</h2>
    <p class="col-sm-6">
      Increases your productivity by harnessing the power of software applications and optimize your work in a computerized workflow,
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/softwareservice.jpeg"/>
    <div class="sep">...</div>
    <h2>Design</h2>
    <p class="col-sm-6">
      Communicate with your clients with neat graphics that speak on your behalf and make them feel the desire to work with you.
         
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/designservice.png"/>
    <div class="sep">...</div>
    <h2>Consulting</h2>
    <p class="col-sm-6">
      Every day we are confronted with challenges that are beyond our area of expertise, we help you face them and empower you to get the most out of your valuable time.
    </p>
    <img class="col-sm-offset-2 col-sm-4" src="images/consultingservice.jpeg"/>


</div>
