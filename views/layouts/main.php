<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?= $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => 'images/favicon.png']);?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => '<img src="images/optidev.png" class="col-lg-11 col-xs-10 col-md-11" />',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
        'brandOptions' => [
            'class' => 'col-xs-8 col-sm-8 col-lg-10',
        ],
    ]);


       echo Nav::widget([
           'options' => ['class' => 'navbar-nav navbar-right navbar-lang'],
           'items' => [
               ['label' => 'FR',  'url' => ['/site/lang?lang=fr']],
               ['label' => 'EN',  'url' => ['/site/lang?lang=en']],

           ],
       ]);
       echo '<br>';
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => \Yii::t('app','Home'), 'url' => ['/site/index']],
            [
              'label'=>\Yii::t('app','Products'),
              'items'=>[
                ['label' => 'Quality Management System','options'=>['class'=>'col-sm-12 sub-services', 'id'=>"harmonyQMS"] , 'url' => ['/site/harmonyqms']],
                ]

            ],
            [
              'label'=>'Services',
              'items'=>[
                ['label' => 'Web design','options'=>['class'=>'col-sm-6 sub-services', 'id'=>"webID"] , 'url' => ['/site/web']],
                ['label' => 'Software applications','options'=>['class'=>'col-sm-6 sub-services', 'id'=>"softID"] , 'url' => ['/site/software']],
                ['label' => 'Graphic design','options'=>['class'=>'col-sm-6 sub-services', 'id'=>"desID"] , 'url' => ['/site/graphics']],
                ['label' => 'Consulting','options'=>['class'=>'col-sm-6 sub-services', 'id'=>"consID"] , 'url' => ['/site/consulting']],
              ]

            ],
            //['label' => 'Product', 'url' => ['/site/products']],
            ['label' => 'Contact', 'url' => ['/site/contact']],

        ],
    ]);
    NavBar::end();


    ?>

    <div class="container-fluid body-height">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p id="copyright" class="pull-left">Copyright &copy; OptiDev  <?= date('Y') ?></p>

        <p id="phone" class="col-xs-10 col-sm-3 col-sm-offset-3"><?= '✆ (+213) (0)668 566 040 ' ?></p>
    </div>
</footer>
<!--Start of Tawk.to Script->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5805e4851e35c727dc05fee7/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->


<!--google analytics-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75334301-2', 'auto');
  ga('send', 'pageview');

</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
